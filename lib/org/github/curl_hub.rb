require 'json'
module Org::Github::CurlHub
  def self.repo_issues access_key, repo
    JSON.parse %x(curl -sk "https://api.github.com/repos/#{repo}/issues?access_token=#{access_key}")
  end
end
