class Org::Github::CurlBucket
  def self.repo_issues username, password, repo
    JSON.parse %x(curl -sk -u '#{username}:#{password}' 'https://bitbucket.org/api/1.0/repositories/#{repo}/issues')
  end
end
