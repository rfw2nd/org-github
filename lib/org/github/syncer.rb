require 'org/github/parser'
require 'org/github/writer'
require 'org/github/curl_hub'
require 'fileutils'
require 'securerandom'
require_relative 'providers'
require_relative 'providers/github'
require_relative 'providers/bitbucket'

class Org::Github::Syncer
  attr_reader :config
  def initialize config
    @config = config
    @org_parser = Org::Github::Parser.new(File.open(config.input_file))
  end
  
  # Proper Formatting
  # I usually prefer the :ID: label so I can get a nice columnview of all issues accross the repos  Optional here.
  # * Github Issues
  #  :PROPERTIES:
  #  :ID: github
  #  :COLUMNS: %4ISSUE %GH_PRI %PRIORITY %TODO %15ITEM %LINK
  #  :END:
  # ** Repo
  #  :PROPERTIES:
  #  :GITHUB_REPO: raymond-wells/org-github
  #  :END:
  # *** Cannot do something
  #    :PROPERTIES:
  #    :ISSUE: 1
  #    :END:
  
  
  def sync_org_file!
    FileUtils.copy(config.input_file, "#{config.input_file}.bak")
    @entries = @org_parser.to_a(with_members: true)
    
    [Org::Github::Providers::Github, Org::Github::Providers::Bitbucket].map(&method(:process_provider))
    File.open(config.input_file, 'w') {|f| f.puts Org::Github::Writer.write_org(@entries)}
    puts status_message
  end
  
  def process_provider klass
    config_key = klass.name.split('::').last.downcase
    puts "Config: #{config_key}"
    provider = klass.new(config[config_key], @entries)
    o_entries = @entries.clone
    @stats ||= {}
    stats = o_entries.map(&provider.method(:sync_info_for)).reduce(&method(:add_hashes))
    @stats = add_hashes(stats, @stats)
  end
  
  private
  
  def status_message
    puts "#{@stats[:updated]} entries updated.  #{@stats[:added]} entries added. #{@stats[:skipped]} with no new updates."
  end
  
  
  def add_hashes a,b
    h = {}
    [a.keys,b.keys].flatten.uniq.each{|k| h[k] = (a[k]||0) + (b[k]||0)}
    h
  end
end
