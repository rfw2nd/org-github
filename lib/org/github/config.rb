require 'json'
require 'ostruct'
module Org
  module Github
    class Config
      attr_reader :data
      attr_accessor :input_file
      def initialize
        @data = JSON.parse(File.read(%Q{#{ENV["HOME"]}/.org-github}))
        @structs = {}
      end
      
      def [] key
        @structs[key] ||= OpenStruct.new(@data[key])
      end
    end
  end
end
