module Org
  module Github
    
    ParseError = Class.new RuntimeError
    
    class PState
      def initialize
        @mode = :default
      end
      attr_accessor :mode
    end
    
    class Parser
      def initialize org_data
        @io = get_io(org_data)
      end
      
      def to_a opts={}
        @org_list ||= get_list_repr(opts)
      end
      
      private
      
      def get_io org_data
        case
        when org_data.is_a?(String) then StringIO.new(org_data)
        else org_data
        end
        
      end
      
      def get_list_repr opts={}
        ret = []
        @io.each_line do |line|
          line = line.chomp
          if line =~ /^\*+/
            ret << @current if @current
            @current = {properties: {}}
            parse_heading(line)
            @state = PState.new
            next
          end
          
          parse_org_line(line) if @current
        end
        ret << @current if @current[:heading]
        compute_members(ret) if opts[:with_members]
        ret
      end
      
      
      def compute_members ary
        ary.each_with_index do |entry, idx|
          entry[:members] = []
          ary.slice(idx+1,ary.length).each do |candidate|
            if candidate[:level] > entry[:level]
              entry[:members] << candidate
            else
              break
            end
          end
        end
      end
      
      def parse_heading line
        todo_data = line.match(/^\*+ (BACKLOG|TODO|IN PROGRESS|BLOCKED|QA|DONE) (.*)/)
        parse_todo_line(todo_data) if todo_data
        line =~ /^(\*+) (.*)/
        @current[:level] = $1.length
        @current[:heading] ||= $2
      end
      
      def parse_todo_line todo_data
        @current[:todo_status] = todo_data[1]
        @current[:heading] = todo_data[2]
      end
      
      def parse_org_line line
        case line
        when /^\s*:PROPERTIES:/ then @state.mode = :properties
        when /^\s*:END:/ then @state.mode = :default
        else send("parse_#{@state.mode}", line)
        end
      end
      
      def parse_default line
        @current[:body] = (@current[:body].empty?) ? [@current[:body], line].join("\n") : line
      end
      
      def parse_properties line
        line =~ /^\s*:(.*): (.*)/
        @current[:properties][$1] = parse_prop_value($2)
      end
      
      def parse_prop_value value
        case value
        when /^<(\d+-\d+-\d+ (Mon|Tue|Wed|Thu|Fri|Sat|Sun))>$/ then Time.parse($1)
        when /^[0-9]+$/ then value.to_i
        else value
        end
      end
    end
  end
end
