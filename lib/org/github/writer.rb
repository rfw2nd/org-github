module Org
  module Github
    class Writer
      def self.write_org entries
        entries.map(&method(:to_org)).join("\n")
      end
      
      private
      
      def self.to_org entry
        todo_part = " #{entry[:todo_status]}" if entry[:todo_status]
        heading = " #{entry[:heading]}"
        level = "*" * entry[:level]
        out = ["#{level}#{todo_part}#{heading}"]
        out << render_properties(entry) unless entry[:properties].empty?
        out << entry[:body] if entry[:body]
        out.join("\n")
      end
      
      def self.render_properties entry
        out = %w(:PROPERTIES:)
        out += entry[:properties].map do |key, value|
          ":#{key}: #{value_to_org(value)}"
        end
        out += %w(:END:)
        out.map{|p| "#{' ' * entry[:level]} #{p}"}.join("\n")
      end
      
      def self.value_to_org val
        case 
        when val.is_a?(Time) then val.strftime("<%Y-%m-%d %a>")
        else val
        end
      end
    end
  end
end
