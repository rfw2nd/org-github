require_relative 'base'
class Org::Github::Providers::Github < Org::Github::Providers::Base
  
  def update_issue issue, member, entry
    update_in_place!(member) do |m|
      m[:properties] = github_properties(issue)
      m[:heading] = github_heading(issue)
      m[:body] = safe_body(member[:level], issue['body'])
    end
  end
  
  def add_issue issue, member, entry
    push_entry(entry, {heading: github_heading(issue), body: safe_body(entry[:level]+1, issue['body']), todo_status: "BACKLOG", level: entry[:level]+1, properties: github_properties(issue)})
  end
  
  def github_properties issue
    {
      "ISSUE" => issue['number'],
        "GH_UPDATED_AT" => issue["updated_at"],
        "LABELS" => issue['labels'].map{|l| l['name']}.join(", "),
        "LINK" => issue['html_url'],
        "UNIQ" => %x(uuidgen).chomp,
        "GH_PRI" => pri_label_for(issue)
    }
  end
  
  def github_heading issue
    issue['title']
  end
  
  def assigned_only issue
    issue['assignee'] && issue['assignee']['login'] == config.login
  end
  
  
  def pri_label_for issue
    lbls = issue['labels'].map do |label|
      config.pri_labels.include?(label['name']) && label['name']
    end.select{|x|x}
    lbls.first || ''
  end
  
  
  def should_sync? entry
    entry[:properties]['GITHUB_REPO']
  end
  
  def fetch_issues_for entry
    Org::Github::CurlHub.repo_issues(config.access_token, entry[:properties]['GITHUB_REPO']).select(&method(:assigned_only))
  end
  
  def issue_for_member? issue, member
    member[:properties]["ISSUE"] == issue['number'].to_i
  end
  
  def needs_update? issue, member
    member[:properties]['GH_UPDATED_AT'] != issue['updated_at']
  end
end
