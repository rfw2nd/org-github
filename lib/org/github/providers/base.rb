class Org::Github::Providers::Base
  attr_accessor :config, :entries
  
  def initialize config, entries
    @config = config
    @entries = entries
  end
  
  def sync_info_for entry
    return {} unless should_sync?(entry)
    issues = fetch_issues_for(entry)
    sync_status = method(:issue_sync_status_for).to_proc.curry[entry]
    @statuses = issues.map(&sync_status)
    @statuses.each{|stat| send(stat[:type], stat[:issue], stat[:member], entry)}
    Hash[{skipped: :fine, updated: :update_issue, added: :add_issue}.map{|stat, key| [stat, compile_stat(key)]}]
  end
  
  def compile_stat key
    @statuses.select{|s| s[:type]==key}.length
  end
  
  def safe_body level, body
    body.split("\n").map do |line|
      "#{' ' * level} #{line.gsub("\r",'')}"
    end
  end
  
  def push_entry entry, member
    i = @entries.find_index{|e| e == entry}+1
    @entries.insert(i, member)
  end
  
  def update_in_place! member
    i = @entries.find_index{|m| m[:properties]["UNIQ"] == member[:properties]["UNIQ"]}
    yield(member)
    @entries[i] = member
  end
  
  def fine issue, member, entry
  end
  
  def issue_sync_status_for entry, issue
    status_type = :fine
    member_issue = method(:issue_for_member?).to_proc.curry[issue]
    member = entry[:members].find(&member_issue)
    if member
      status_type = :update_issue  if needs_update?(issue, member)
    else
      status_type = :add_issue
    end
    
    { issue: issue, member: member, type: status_type  }
  end
  
end
