require_relative 'base'
require_relative '../curl_bucket'
require 'uri'
class Org::Github::Providers::Bitbucket < Org::Github::Providers::Base
  def update_issue issue, member, entry
    update_in_place!(member) do |m|
      m[:properties] = bitbucket_properties(issue, entry)
      m[:heading] = issue['title']
      m[:body] = safe_body(member[:level], issue['content'])
    end
  end
  
  def add_issue issue, member, entry
    member = {heading: issue['title'], body: safe_body(entry[:level]+1, issue['content']), todo_status: bb_todo(issue), level: entry[:level]+1, properties: bitbucket_properties(issue, entry)}
    push_entry(entry, member)
  end
  
  # ColumnView:
  # :COLUMNS: %4LOCAL_ID %BB_PRI %PRIORITY %TODO %15ITEM %LINK
  
  def bitbucket_properties issue, entry
    {
      "LOCAL_ID" => issue['local_id'],
      "BB_LAST_UPDATED" => issue['utc_last_updated'],
      "BB_PRI" => issue['priority'],
      "BB_TYPE" => issue['type'],
      "LINK" => bb_link_for(issue, entry),
      "UNIQ" => %x(uuidgen).chomp
    }
  end
  
  def bb_todo issue
    case issue['status']
    when *%w(new) then "BACKLOG"
    when *%w(resolved wontfix invalid) then "DONE"
    end
  end
  
  def bb_link_for issue, entry
    slug = URI.escape issue['title'].downcase.gsub(' ', '-')
    
    "https://bitbucket.org/#{entry[:properties]['BITBUCKET_REPO']}/issue/#{issue['local_id']}/#{slug}"
  end
  
  def should_sync? entry
    entry[:properties]['BITBUCKET_REPO']
  end
  
  def fetch_issues_for entry
    Org::Github::CurlBucket.repo_issues(config.username, config.password, entry[:properties]['BITBUCKET_REPO'])['issues']
  end
  
  def issue_for_member? issue, member
    member[:properties]["LOCAL_ID"] == issue['local_id'].to_i
  end
  
  def needs_update? issue, member
    member[:properties]['BB_LAST_UPDATED'] != issue['utc_last_updated']
  end
end
